import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabRoutingModule } from './lab-routing.module';
import { LabComponent } from './lab.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';
import {SharedModule} from '../../../shared/sharedModule';
import { AccordionModule } from 'primeng/accordion';
import { RippleModule } from 'primeng/ripple';
import { SpeedDialModule } from 'primeng/speeddial';

@NgModule({
  declarations: [
    LabComponent
  ],
  imports: [
    CommonModule,
    LabRoutingModule,
    MessagesModule,
    MessageModule,
    ButtonModule,
    SharedModule,
    AccordionModule,
    RippleModule,
    SpeedDialModule

  ]
})
export class LabModule { }
