import { Component, OnInit, Input, AfterViewInit , OnChanges, SimpleChanges} from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
    selector: 'app-progress-note',
    templateUrl: './progress-note.component.html',
    styleUrls: ['./progress-note.component.scss'],
})
export class ProgressNoteComponent implements OnInit,OnChanges {
    @Input() parentData: any;
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;
    optionSubjective: any;
    optionsAssessment: any;
    optionPlan: any;
    showProgressNote: boolean = false;
    optionSubjectiveSelected: any;
    optionObjective: any = [];
    optionObjectiveSelected: any;
    optionAssessmentSelected: any;
    optionPlanSelected: any;
    progressValueS: any;
    progressValueO?: any;
    progressValueA?: any;
    progressValueP?: any;
    progressValueN?: any;

    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];
    progress_note: any[] = [];
    // progressValue: any = [];
    progressValue: { label: string }[] = [];
    dxLoading: boolean = false;

    // doctor order id
    doctorOrderData: any;
    is_new_order: boolean = true;
    is_success: boolean = false;

    // user
    userData: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
        console.log('queryParamsData:', this.queryParamsData);

        // let _data = sessionStorage.getItem('doctor_order');
        // if (_data != 'undefined' && _data != null) {
        //     this.doctorOrderData = JSON.parse(_data);
        //     this.is_new_order = false;
        // } else {
        //     this.is_new_order = true;
        // }

        let _user = sessionStorage.getItem('userData');
        if (_user != 'undefined' && _user != null) {
            let user = JSON.parse(_user!);
            this.userData = user;
        } else {
            this.userData = null;
        }
    }
    ngOnInit(): void {
        console.log(this.parentData);
        this.is_new_order = this.parentData.isNewOrder;
        this.doctorOrderData =this.parentData.doctor_order;
        console.log(this.doctorOrderData);

        console.log('init progressNote');
    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes['parentData'] && !changes['parentData'].firstChange) {
          // Data changed after initialization, update flag
        //   this.dataReceived = true;
        console.log(this.parentData);

        }
      }
    getProgressNote() {
        // console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        this.dxLoading = true;
        try {
            let response = await this.lookupService.getDxStardingorder();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
            this.dxLoading = false;
        } catch (error: any) {
            console.log('getDx' + `${error.code} - ${error.message}`);
            this.dxLoading = false;
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            // console.log('get doctor order :',data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                          .setLocale('th')
                          .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            // console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    async dxlistClick(id: any, name: any) {
        console.log(id);

        try {
            let response = await this.lookupService.getStanding(id);
            //  let progessnoteData: any = response.data.data;
            // this.optionsDrugsUsage = response.data.data;
            console.log(response);
            this.dxName = name;
            let progessnoteData = response.data.data;
            this.optionSubjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'S'
            );
            this.optionObjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'O'
            );
            this.optionsAssessment = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'A'
            );
            this.optionPlan = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'P'
            );

            this.showProgressNote = true;
            this.showDx = false;

            // this.optionSubjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'S')
            // this.optionObjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'O')
            // this.optionsAssessment = progessnoteData.filter((item: any) => item.progress_note_type_code == 'A')
            // this.optionPlan = progessnoteData.filter((item: any) => item.progress_note_type_code == 'P')
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    optionSubjectClick(e: any) {
        if (e.target.checked) {
            this.optionSubjectiveSelected.push(e.target.value);
        } else {
            this.optionSubjectiveSelected.splice(
                this.optionSubjectiveSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }
        // console.log(this.optionSubjectiveSelected);
    }
    optionObjectClick(e: any) {
        if (e.target.checked) {
            this.optionObjectiveSelected.push(e.target.value);
        } else {
            this.optionObjectiveSelected.splice(
                this.optionObjectiveSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionObjectiveSelected);
    }
    optionAssessmentClick(e: any) {
        if (e.target.checked) {
            this.optionAssessmentSelected.push(e.target.value);
        } else {
            this.optionAssessmentSelected.splice(
                this.optionAssessmentSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionAssessmentSelected);
    }

    optionPlanClick(e: any) {
        if (e.target.checked) {
            this.optionPlanSelected.push(e.target.value);
        } else {
            this.optionPlanSelected.splice(
                this.optionPlanSelected.indexOf(e.target.value),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionPlanSelected);
    }

    async checkOrder() {
        if (this.is_new_order) {
            var date2 = DateTime.now();
            var date3 = DateTime.now();
            const date5 = date2.toFormat('yyyy-MM-dd');
            const time1 = date3.toFormat('HH:mm:ss');
            let data = {
                doctor_order: [{
                    admit_id: this.queryParamsData,
                    doctor_order_date: date5,
                    doctor_order_time: time1,
                    doctor_order_by: this.userData.id || null,
                    is_confirm: false,
                }],
            };
            console.log('send data:', data);
            try {
                let rs = await this.doctorOrderService.saveDoctorOrder(data);
                console.log('response:', rs);
                this.doctorOrderData = rs.data.data.doctor_order;
                this.is_new_order = false;
                // sessionStorage.setItem(
                //     'doctor_order',
                //     JSON.stringify(this.doctorOrderData)
                // );
                console.log('doctorOrderData:', this.doctorOrderData);
                this.is_success = true;
            } catch (error) {
                console.log('save Doctor Order:' + error);
                this.is_success = false;
            }
        } else {
            this.is_success = true;
        }
    }

    async addProgressNote(type: string) {
        await this.checkOrder(); // check if doctor order is exist

        let progress_note: any; // set progress note
        if (this.is_success) {
            // select type of progress note
            if (type == 'S') {
                this.progress_note_subjective.push({
                    label: this.progressValueS,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_subjective) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    subjective: _progress_note,
                };
                this.progressValueS = '';
            } else if (type == 'O') {
                this.progress_note_objective.push({
                    label: this.progressValueO,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_objective) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    objective: _progress_note,
                };
                this.progressValueO = '';
            } else if (type == 'A') {
                this.progress_note_assertment.push({
                    label: this.progressValueA,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_assertment) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    assertment: _progress_note,
                };
                this.progressValueA = '';
            } else if (type == 'P') {
                console.log(this.progressValueP );
                this.progress_note_plan.push({ label: this.progressValueP });
                let _progress_note: any = '';
                
                for (let i of this.progress_note_plan) {
                    _progress_note =[_progress_note + i.label];
                }
               
                    // _progress_note = _progress_note + this.progressValueP ;
                
                progress_note = {
                    plan: _progress_note,
                };
                this.progressValueP = '';
            } else {
                this.progress_note.push({ label: this.progressValueN });
                let _progress_note: any = [];
                for (let i of this.progress_note) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    note: _progress_note,
                };
                this.progressValueN = '';
            }
            await this.saveProgressNote(progress_note); // save progress note
        } else {
            alert('ไม่สามารถบันทึกข้อมูลได้');
        }
    }
    async saveProgressNote(datas: any) {
        // set data to save
        let data = {
            doctor_order: this.doctorOrderData,
            progress_note: datas,
        };
        console.log('progress_note:', data);

        try {
            const response = await this.doctorOrderService.saveDoctorOrder(
                data
            );
            console.log(response);
        } catch (error) {
            console.log('saveDoctorOrder:' + error);
        }
    }
    async removeProgressNote(label: string, type: string) {
        if (type == 'S') {
            this.progress_note_subjective =
                this.progress_note_subjective.filter(
                    (item) => item.label !== label
                );
            await this.addProgressNote('S');
        } else if (type == 'O') {
            this.progress_note_objective = this.progress_note_objective.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('O');
        } else if (type == 'A') {
            this.progress_note_assertment =
                this.progress_note_assertment.filter(
                    (item) => item.label !== label
                );
            await this.addProgressNote('A');
        } else if (type == 'P') {
            this.progress_note_plan = this.progress_note_plan.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('P');
        } else {
            this.progress_note = this.progress_note.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('N');
        }
    }

 

    async saveStandingOrder() {
        alert('saveStandingOrder');
        this.sidebarVisible2 = false;
    }
}
