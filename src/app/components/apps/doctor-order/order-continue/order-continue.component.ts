import { Component, Input, OnInit } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
}

interface OptionDrugsUsage {
    id: string;
    code: string;
    name: string;
    is_active: boolean;
    description: string;
    create_date?: string;
    create_by?: string;
    modify_date?: string;
    modify_by?: string;
    time_per_day?: string;
}

interface OptionItems {
    id: string;
    name: string;
    default_usage: string;
    item_type_id: string;
}

@Component({
    selector: 'app-order-continue',
    templateUrl: './order-continue.component.html',
    styleUrls: ['./order-continue.component.scss'],
})
export class OrderContinueComponent implements OnInit {
    @Input() parentData: any;

    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;

    // order data
    orders: any[] = [];

    orderContinueV: any;

    orderContinueValue: any = [];

    // list of drugs usage
    optionsDrugsUsage: OptionDrugsUsage[] = [];

    // list of all items
    optionsAllItems: OptionItems[] = [];

    selectedItems: any;
    filteredItems: any | undefined;

    selectedUsage: any = '';
    filteredUsage: any | undefined;

    // doctor order id
    doctorOrderData: any;
    is_new_order: boolean = true;
    is_success: boolean = false;

    // user
    userData: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;

        // get doctor order
        // let _data = sessionStorage.getItem('doctor_order');
        // if (_data != 'undefined' && _data != null) {
        //     this.doctorOrderData = JSON.parse(_data);
        //     this.is_new_order = false;
        // } else {
        //     this.is_new_order = true;
        // }
        // get user data
        let _user = sessionStorage.getItem('userData');
        if (_user != 'undefined' && _user != null) {
            let user = JSON.parse(_user!);
            this.userData = user;
        } else {
            this.userData = null;
        }
    }
    ngOnInit(): void {
        // console.log('progressNote');
        console.log(this.parentData);
        this.is_new_order = this.parentData.isNewOrder;
        this.doctorOrderData =this.parentData.doctor_order;
        console.log(this.doctorOrderData);
        this.getItem();
        this.getMedicineUsage();
    }

    // get medicine usage
    async getMedicineUsage() {
        try {
            let response = await this.doctorOrderService.getMedusage();
            this.optionsDrugsUsage = response.data.data;
            console.log('med usage:', this.optionsDrugsUsage);
        } catch (error: any) {
            console.log(error);
        }
    }

    // get list all items
    async getItem() {
        try {
            let response = await this.doctorOrderService.getLookupItem();
            this.optionsAllItems = response.data.data;
            console.log('Items:', this.optionsAllItems);
        } catch (error: any) {
            console.log(error);
        }
    }

    getProgressNote() {
        console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        try {
            let response = await this.lookupService.getDx();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                          .setLocale('th')
                          .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    filterItems(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsAllItems.length; i++) {
            let item = this.optionsAllItems[i];
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItems = filtered;
    }

    filterUsage(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsDrugsUsage.length; i++) {
            let item = this.optionsDrugsUsage[i];
            if (
                item.name.toLowerCase().indexOf(query) !== -1 ||
                item.code.toLowerCase().indexOf(query) !== -1
            ) {
                filtered.push(item);
            }
        }
        this.filteredUsage = filtered;
    }

    selectedItemsClear() {
        this.selectedItems = '';
    }
    selectedUsageClear() {
        this.selectedUsage = '';
    }

    // check if doctor order is exist
    // check is new order
    async checkOrder() {
        if (this.is_new_order) {
            var date2 = DateTime.now();
            var date3 = DateTime.now();

            const date5 = date2.toFormat('yyyy-MM-dd');
            const time1 = date3.toFormat('HH:mm:ss');

            let data = {
                doctor_order: [{
                    admit_id: this.queryParamsData,
                    doctor_order_date: date5,
                    doctor_order_time: time1,
                    doctor_order_by: this.userData.id || null,
                    is_confirm: false,
                }],
            };
            // console.log('data:',data);
            try {
                let rs = await this.doctorOrderService.saveDoctorOrder(data);
                // console.log('response:',rs);
                this.doctorOrderData = rs.data.data.doctor_order;
                this.is_new_order = false;
                // sessionStorage.setItem(
                //     'doctor_order',
                //     JSON.stringify(this.doctorOrderData)
                // );
                // console.log('doctorOrderData:',this.doctorOrderData);
                this.is_success = true;
            } catch (error) {
                console.log('save Doctor Order:' + error);
                this.is_success = false;
            }
        } else {
            this.is_success = true;
        }
    }

    async saveOrder() {
        await this.checkOrder();
        console.log('is_success:', this.is_success);
        console.log('selectedItems:', this.selectedItems);

        let orderData: any = {};

        if (this.is_success) {
            // check if selectedItems and selectedUsage is empty
            if (this.selectedItems && this.selectedUsage == '') {
                orderData = {
                    order_type_id: 2,
                    item_type_id: this.selectedItems.item_type_id,
                    item_id: this.selectedItems.id,
                    item_name: this.selectedItems.name,
                    is_confirm: false,
                };
            } else if (this.selectedItems && this.selectedUsage) {
                orderData = {
                    order_type_id: 2,
                    item_type_id: this.selectedItems.item_type_id,
                    item_id: this.selectedItems.id,
                    item_name: this.selectedItems.name,
                    medicine_usage_code: this.selectedUsage.code || null,
                    medicine_usage_extra:
                        this.selectedUsage.description || null,
                    is_confirm: false,
                };
            } else {
                alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
                return;
            }

            let data = {
                doctor_order: this.doctorOrderData,
                orders: [orderData],
            };

            console.log('data:', data);
            try {
                // console.log('send data:', data);
                let res: any = await this.doctorOrderService.saveDoctorOrder(
                    data
                );
                console.log('response:', res);
                let _order_id = res.data.data.order.id;

                // push to label
                this.orderContinueValue.push({
                    id: _order_id,
                    items: this.selectedItems,
                    usage: this.selectedUsage || '',
                });
            } catch (error) {
                console.log('save Doctor Order:' + error);
            }
        } else {
            alert('ไม่สามารถบันทึกข้อมูลได้');
        }
        console.log('bandage:',this.orderContinueValue);
        this.selectedItems = '';
        this.selectedUsage = '';
    }

    async removeOrder(id: any) {
        console.log('remove order id:', id);
        try {
            let rs = await this.doctorOrderService.removeOrders(id);
            console.log('response:', rs);
            // this.getDoctorOrderById();

            // remove from label
            this.orderContinueValue = this.orderContinueValue.filter(
                (item: any) => item.id !== id
            );
        } catch (error) {
            console.log('remove Doctor Order:' + error);
        }
    }
}
